package Tests;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

public class TestBase{
    public static WebDriver driver;

    @BeforeTest
    public void StartDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/Drivers/chromedriver.exe");

    }
    @BeforeMethod(onlyForGroups = "FirstTimeOpenURL")
    public void Navigation() {

        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
        driver.get("https://www.booking.com");

    }


    @AfterTest
    public void stopDriver() {
        driver.quit();

    }

}