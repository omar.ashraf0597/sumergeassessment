package Tests;

import Pages.BookingDetailsPage;
import Pages.BookingHomePage;
import Pages.HotlesPage;
import data.ExcelReader;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.ArrayList;


public class BookingTest extends TestBase{

    BookingHomePage booking;
    HotlesPage  hotel;
    BookingDetailsPage detailsPage;


    @DataProvider(name = "DataFromExcel")
    public Object[][] InputData() throws IOException {
        // get data from ExcelReader class
        ExcelReader er = new ExcelReader();
        return er.getExcelData();
    }


    // Test to Read data from Excel file ,  and click on search button.
   @Test(priority = 1, dataProvider="DataFromExcel", groups = "FirstTimeOpenURL")
    public void Fill_Trip_Data(String Location, String CheckInMonth, String CheckInDay,
                             String CheckOutMonth, String CheckOutDay) throws InterruptedException{
        booking = new BookingHomePage(driver);
        booking.FillTripData(driver,Location,CheckInMonth,CheckInDay,CheckOutMonth,CheckOutDay);
        Thread.sleep(3000);
        booking.SearchForTrip();
    }


    // Test to check the Tolip hotel and see the availability of it
    @Test(priority = 2,dependsOnMethods = "Fill_Trip_Data")
    public void Check_Hotel() throws InterruptedException {
        hotel = new HotlesPage(driver);
        hotel.CheckTolipHotel(driver);
        hotel.SelectTolipHotel(driver);
        ArrayList<String> tabs_windows = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs_windows.get(1));
    }

    // Test to Assert on Hotel name
    @Test(priority = 3,dependsOnMethods = "Check_Hotel")
    public void Assert_Hotel_Name(){
        hotel = new HotlesPage(driver);
        hotel.AssertHotelName(driver);
    }

    //Test to Fill rooms's data
    @Test(priority = 4,dependsOnMethods = "Assert_Hotel_Name")
    public void Reserve_Room() throws InterruptedException {
        detailsPage = new BookingDetailsPage(driver);
        Thread.sleep(3000);
        detailsPage.SelectRoom(driver);
        Thread.sleep(3000);

    }

    //Assert Dates and click on Reserve button
    @Test (priority = 5,dependsOnMethods = "Reserve_Room")
    public void AssertDates(){
        detailsPage = new BookingDetailsPage(driver);
        detailsPage.AssertDates(driver);
        detailsPage.ReserveTrip();
    }

}

