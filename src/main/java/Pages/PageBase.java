package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.WheelInput;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;

public class PageBase {


    // create constructor
    public PageBase(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    // To click on button
    public void clickButton(WebElement button) {
        button.click();
    }

    // To Send keys to elements
    public void setTextElementText(WebElement textElement, String value) {
        textElement.sendKeys(value);
    }

    //Select specific value from dropdown
    public void SelectFromLOV(WebElement optionList, String value) {
        Select selectOptions = new Select(optionList);
        selectOptions.selectByValue(value);
    }

    //Scroll to element
    public void ScrollToIndexByXpath(WebDriver driver , String Selector) {
        Actions Scroll = new Actions(driver);
        WebElement S = driver.findElement(By.xpath(Selector));
        Scroll.moveToElement(S).perform();
    }

    // Scroll Vertical
    public void ScrollVertical (WebDriver driver,String scrollArea,int YPixel)  {
        WebElement iframe = driver.findElement(By.xpath(scrollArea));
        WheelInput.ScrollOrigin scrollOrigin = WheelInput.ScrollOrigin.fromElement(iframe);
        new Actions(driver).scrollFromOrigin(scrollOrigin,0, YPixel).perform();
    }


    //Press ESC from keyboard
    public void pressESC (WebDriver driver)  {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE).build().perform();
    }

    //Press Tab from keyboard
    public void pressTAB (WebDriver driver)  {
        Actions action = new Actions(driver);
        action.sendKeys(Keys.TAB).build().perform();
    }
}
