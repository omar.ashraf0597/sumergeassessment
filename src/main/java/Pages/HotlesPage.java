package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.junit.Assert.assertEquals;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HotlesPage extends PageBase {
    public HotlesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(className = "a15b38c233")
    List<WebElement> hotels;

    @FindBy(xpath = "//button[@aria-label='Next page']//span[@class='fcd9eec8fb bf9a32efa5']//*[name()='svg']")
    WebElement nextPage;

    @FindBy(xpath = "//*[contains(text(), 'Tolip Hotel Alexandria')]")
    WebElement tolipHotel;

    @FindBy(xpath = "//*[@id=\"hp_hotel_name\"]/div/h2")
    WebElement hotelTitle;


    // Function to search for Tolip hotel in first page , and if not exists will click on next page icon and search for tolip
    // Till catch the hotel.
    public void CheckTolipHotel(WebDriver driver) throws InterruptedException {

        boolean checkElementsInScreen = false;

        for (int i = 0; i < hotels.size(); i++) {
            if (hotels.get(i).getText().contains("Tolip"))
            {
                System.out.println("Hotel is: " + hotels.get(i).getText());
                checkElementsInScreen = true;
                break;
            }
        }
        if (checkElementsInScreen == false) {
            Thread.sleep(3000);
            ScrollToIndexByXpath(driver, "//button[@aria-label='Next page']//span[@class='fcd9eec8fb bf9a32efa5']//*[name()='svg']");
            //ScrollVertical(driver,"//button[@aria-label='Next page']//span[@class='fcd9eec8fb bf9a32efa5']//*[name()='svg']",200);
            driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
            clickButton(nextPage);
            driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
            CheckTolipHotel(driver);
        }
    }


    // Scroll to Tolip hotel and click on availability button
    public void SelectTolipHotel(WebDriver driver){
        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        ScrollVertical(driver,"//*[contains(text(), 'Tolip Hotel Alexandria')]",300);
        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        clickButton(tolipHotel);
        }


        // Assert the Hotel name is "Tolip Hotel Alexandria"
    public void AssertHotelName(WebDriver driver)
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String actualName = hotelTitle.getText();
        String expectedName = "Tolip Hotel Alexandria";
        Assert.assertEquals(expectedName,actualName);
        System.out.println(actualName);
    }

    }