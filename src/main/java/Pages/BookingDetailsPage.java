package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class BookingDetailsPage extends PageBase{
    public BookingDetailsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//tr[@class='js-rt-block-row e2e-hprt-table-row hprt-table-cheapest-block hprt-table-cheapest-block-fix js-hprt-table-cheapest-block ']//label[3]//ul[1]//li[1]//span[1]")
    WebElement firstBed;

    @FindBy(xpath = "//select[@id='hprt_nos_select_78883120_91939502_0_33_0_131741']")
    WebElement firstRoom;

    @FindBy(xpath = "//span[@class='bui-button__text js-reservation-button__text']")
    WebElement iWillReserveButton;

    @FindBy(xpath = "//*[@id=\"hp_availability_style_changes\"]/div[3]/div/div/form/div[1]/div[1]/div/div/button[1]")
    WebElement actualFromDay;

    @FindBy(xpath = "//*[@id=\"hp_availability_style_changes\"]/div[3]/div/div/form/div[1]/div[1]/div/div/button[2]")
    WebElement actualToDay;


    //Select the bed and number of room that you want
    public void SelectRoom(WebDriver driver){

        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        ScrollToIndexByXpath(driver,"//span[@class='bui-button__text js-reservation-button__text']");
        ScrollVertical(driver,"//span[@class='bui-button__text js-reservation-button__text']",500);

        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        clickButton(firstBed);
        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        SelectFromLOV(firstRoom,"1");
        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
    }


    // Function to Assert for retrieved Dates is matched with entered dates
    public void AssertDates(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String FromDay = actualFromDay.getText();
        String ToDay = actualToDay.getText();
        System.out.println("Actual From Date is " + FromDay);
        System.out.println("Actual To Date is " + ToDay);

        // To get the entered date from first tab
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(0));
        WebElement element = driver.findElement(By.tagName("header"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        String FromDateFirstTab = BookingHomePage.GetFromDay();
        String ToDaTeFirstTab = BookingHomePage.GetToDay();
        driver.switchTo().window(tabs.get(1));

        // Assert for retrieved Dates is matched with entered dates
        Assert.assertEquals(FromDay, FromDateFirstTab);
        Assert.assertEquals(ToDay, ToDaTeFirstTab);
    }


    public void ReserveTrip()
    {
        clickButton(iWillReserveButton);
    }

}


