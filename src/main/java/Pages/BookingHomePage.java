package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class BookingHomePage extends PageBase{

    public BookingHomePage(WebDriver driver) {
        super(driver);
    }



    @FindBy(name = "ss")
    WebElement location;
    @FindBy(xpath = "//select[@data-name='year-month']")

    WebElement checkInMonth;

    @FindBy(xpath = "//select[@data-name='day']")
    WebElement checkInDay;

    @FindBy (xpath = "//select[@data-name='year-month']")
    WebElement checkOutMonth;

    @FindBy(xpath = "//select[@data-name='day']")
    WebElement checkOutDay;
    @FindBy (xpath = "//span[normalize-space()='Search']")
    WebElement searchbtn;

    @FindBy(xpath = "//button[@data-testid='date-display-field-start']")
    static
    WebElement fromDay;

    @FindBy(xpath = "//button[@data-testid='date-display-field-end']")
    static
    WebElement toDay;


    // Fill Trip's data in Home page
    public void FillTripData(WebDriver driver, String Location, String CheckInMonth ,
                             String CheckInDay, String CheckOutMonth,String CheckOutDay) throws InterruptedException {
        Thread.sleep(5000);
        pressESC(driver);

        clickButton(location);
        setTextElementText(location,Location);
        pressTAB(driver);

        SelectFromLOV(checkInMonth,CheckInMonth);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        pressTAB(driver);

        SelectFromLOV(checkInDay,CheckInDay);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        pressTAB(driver);

        SelectFromLOV(checkOutMonth,CheckOutMonth);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        pressTAB(driver);

        SelectFromLOV(checkOutDay,CheckOutDay);
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        pressTAB(driver);
    }

    // Click on search button
    public void SearchForTrip(){
        clickButton(searchbtn);
    }

    // Get the Date's values that entered from user
    public static String GetFromDay()
    {
        String FromDay = fromDay.getText();
        return FromDay;
    }
    public static String GetToDay()
    {
        String ToDay = toDay.getText();
        return ToDay;
    }

}
